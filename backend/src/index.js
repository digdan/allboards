import dotenv from "dotenv";
import {ApolloServer} from "apollo-server";
import {PubSub} from "graphql-subscriptions";
import {createServer} from "http";
import {SubscriptionServer} from "subscriptions-transport-ws";
import {execute, subscribe} from "graphql";
import {makeExecutableSchema} from "graphql-tools";
import typeDefs from "./graphql/typeDefs";
import resolvers from "./graphql/resolvers/index";
import connectDB from "./config/database";

dotenv.config();

const pubsub = new PubSub();
const port = process.env.PORT || 8080;
const WS_PORT = 8081;

// Connect Database
connectDB();

const server = new ApolloServer({
  typeDefs,
  resolvers,
  context: async ({ req }) => ({ req }),
  uploads: false
});

const websocketServer = createServer((request, response) => {
  response.writeHead(404);
  response.end();
});

const schema = makeExecutableSchema({
  typeDefs,
  resolvers
});


websocketServer.listen(WS_PORT, () => console.log(
  `Websocket Server is now running on http://localhost:${WS_PORT}`
));

const subscriptionServer = SubscriptionServer.create(
  {
    schema,
    execute,
    subscribe,
  },
  {
    server: websocketServer,
    path: '/graphql',
  },
);

server.listen(port).then(({ url }) => {
  console.log(`Server ready at ${url}`);
});
