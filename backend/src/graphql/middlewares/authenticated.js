require("dotenv").config();
import jwt from "jsonwebtoken";
import generateToken from "../../util/generateToken";
import User from "../../models/User";

const secret = process.env.JWT_SECRET || "YOURSECRET";

export default async ({ context, newToken, dontPopulate }) => {
  const authHeader = context.req.headers.authorization;
  let user = null;
  let token = null;
  if (authHeader) {
    /* eslint prefer-destructuring: ["error", {VariableDeclarator: {object: true}}] */
    token = authHeader.split("JWT ")[1];
    if (token) {
      user = jwt.verify(token, secret);
    }
  }
  if (newToken) {
    const newUser = await User.findById(user.id).populate(
      "friends",
      "firstName lastName avatarImage id username"
    );

    token = generateToken(newUser);
    user = newUser;
  }
  if (newToken && dontPopulate) {
    const newUser = await User.findById(user.id);
    token = generateToken(newUser);
    user = newUser;
  }

  return {
    token,
    user,
  };
};
