import testResolvers from "./hello";
import usersResolvers from "./users";
import postsResolvers from "./posts";
import commentsResolvers from "./comments";
import likesResolvers from "./likes";
import notificationsResolvers from "./notifications";
import aboutResolvers from "./about";
import imageResolvers from "./images";
import friendsResolvers from "./friends";
import messagesResolvers from "./messages";
import threadsResolvers from "./threads";
import newsfeedResolvers from "./newsfeed";

export default {
  Query: {
    ...testResolvers.Query,
    ...usersResolvers.Query,
    ...postsResolvers.Query,
    ...likesResolvers.Query,
    ...notificationsResolvers.Query,
    ...aboutResolvers.Query,
    ...imageResolvers.Query,
    ...friendsResolvers.Query,
    ...messagesResolvers.Query,
    ...threadsResolvers.Query,
    ...newsfeedResolvers.Query,
  },
  Mutation: {
    ...usersResolvers.Mutation,
    ...postsResolvers.Mutation,
    ...commentsResolvers.Mutation,
    ...likesResolvers.Mutation,
    ...notificationsResolvers.Mutation,
    ...aboutResolvers.Mutation,
    ...imageResolvers.Mutation,
    ...friendsResolvers.Mutation,
    ...messagesResolvers.Mutation,
    ...threadsResolvers.Mutation,
  },
  Subscription: {
    ...notificationsResolvers.Subscription,
    ...messagesResolvers.Subscription,
    ...postsResolvers.Subscription,
  },
};
